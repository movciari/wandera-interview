import string
import random
from time import sleep
from redis import Redis
from requests_counter.rollingwindow import add


def test_add_request():
    redis = Redis()
    key = ''.join(random.choice(string.ascii_uppercase) for _ in range(10))
    window = 5  # 5 seconds time window to make tests reasonably fast
    for i in range(10):
        assert add(redis, key, window) == i + 1
        # index index 0 is first request for this key
    sleep(3)
    for i in range(10):
        assert add(redis, key, window) == i + 11
        # because we already made 10 requests 3 seconds ago
    sleep(3)
    for i in range(10):
        assert add(redis, key, window) == i + 11
        # checking that first 10 requests really expired
    sleep(6)
    assert add(redis, key, window) == 1
    # checking that all requests expired
