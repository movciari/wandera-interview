import os

REDIS_URL = os.environ.get('REDIS_URL') or "redis://redis:6379/0"
DEBUG = os.environ.get('DEBUG') != 'false'
