from flask import request
from flask import jsonify
from requests_counter.init import app, redis
from requests_counter.rollingwindow import add


@app.route('/', defaults={'path': ''},
           methods=['GET', 'HEAD', 'POST', 'PUT', 'DELETE'])
@app.route('/<path:path>',
           methods=['GET', 'HEAD', 'POST', 'PUT', 'DELETE'])
def root(path):
    ip = request.environ.get('HTTP_X_FORWARDED_FOR', request.remote_addr)
    count = add(redis, ip, 20)
    debug = app.config['DEBUG']

    return (jsonify({'ip': ip, 'count': count}), 200) if debug \
        else (str(count), 200)


if __name__ == '__main__':
    app.run(processes=4, use_reloader=False, debug=False, host='0.0.0.0')
