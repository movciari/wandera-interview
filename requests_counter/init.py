from flask import Flask
from flask_redis import FlaskRedis

__author__ = 'movciari'

app = Flask(__name__)
app.config.from_object('config')

redis = FlaskRedis(app, strict=False)
